package com.tw.bronze.question2;

import java.util.HashMap;
import java.util.List;

public class ReceiptPrinter {
    private final HashMap<String, Product> products =
        new HashMap<>();

    public ReceiptPrinter(List<Product> products) {
        // TODO:
        //   Please implement the constructor
        // <-start-
        products.forEach(product -> {
            this.products.put(product.getId(), product);
        });
        // --end->
    }

    public int getTotalPrice(List<String> selectedIds) {
        // TODO:
        //   Please implement the method
        // <-start-
        if (selectedIds == null) {
            throw new IllegalArgumentException();
        }
        if (selectedIds.isEmpty()) {
            return 0;
        }
        int totalPrice = 0;
        for (int i = 0; i < selectedIds.size(); i++) {
            totalPrice += this.products.get(selectedIds.get(i)).getPrice();
        }
        return totalPrice;
        // --end->
    }
}

