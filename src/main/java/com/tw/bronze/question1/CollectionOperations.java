package com.tw.bronze.question1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CollectionOperations {
    /**
     * This method will remove all the duplicated `String` values from a
     * collection.
     *
     * @param values The collection which may contain duplicated values
     * @return A new array removing the duplicated values.
     */
    public static String[] distinct(Iterable<String> values) {
        // TODO:
        //  Please implement the method. If you have no idea what Iterable<T>
        //  is, please refer to the unit test, or you can have a search on
        //  the internet, or you can find it in our material.
        // <-start-
        if (values == null) {
            throw new IllegalArgumentException();
        }
        List<String> nonDuplicate = new ArrayList<String>();
        String[] nonDuplicateArray = new String[nonDuplicate.size()];
        values.forEach(value -> {
            nonDuplicate.add(value);
        });

        if (nonDuplicate.isEmpty()) {
            return nonDuplicateArray;
        }
        values.forEach(value -> {
            if (nonDuplicate.indexOf(value) != nonDuplicate.lastIndexOf(value)) {
                nonDuplicate.remove(nonDuplicate.lastIndexOf(value));
            }
        });
        Collections.sort(nonDuplicate);
        return nonDuplicate.toArray(nonDuplicateArray);
        // --end->
    }
}
